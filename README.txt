You will need (at least):
* Python 2.7
* AutoIt 3

And the following python libraries (pip install):
* mido
* rtmidi

And of course a suitable Midi device. The code was tested with:
* Novation Launchpad Pro (in programmer mode)
