import time
import Queue
import thread

import mido


class Color:
    """Simple rgb color class"""
    def __init__(self, rgb_or_r, g=None, b=None):
        if g is None and b is None:
            self.r = (rgb_or_r & 0xFF0000) >> 16
            self.g = (rgb_or_r & 0x00FF00) >> 8
            self.b = (rgb_or_r & 0x0000FF)
        else:
            self.r = rgb_or_r
            self.g = g
            self.b = b
    #
    
    def mix(self, other, x):
        """Mix this color with x% from other"""
    
        u = 1-x
        v = x
    
        return Color(
            int(self.r*u + other.r*v),
            int(self.g*u + other.g*v),
            int(self.b*u + other.b*v),
        )
    #
    
    def __repr__(self):
        return "Color(r=%s, g=%s, b=%s)" % (self.r, self.g, self.b)
    #
#


# Colors averaged from the board in game
colors_real = map(Color, [
    0x2C572B,
    0x4B2A29,
    0x56622A,
    0x26355D,
    0x584E2D,
    0x3D2D5F,
    0x23555B,
    0x513A5D,
])

# The previous colors, but saturated
colors_saturated = map(Color, [
    0x025700,
    0x4B0300,
    0x4D6200,
    0x00195D,
    0x584400,
    0x1F005F,
    0x00525B,
    0x44165D,
])

# An arbitrary mix of the above colors
colors = [colors_real[i].mix(colors_saturated[i], 0.8) for i in range(len(colors_real))]


class BellThing:
    def __init__(self):
        # These might need adjusting
        self.device_in = "MIDIIN2 (Launchpad Pro) 3"
        self.device_out = "MIDIOUT2 (Launchpad Pro) 4"
        
        # Maps the midi notes to color indexes and keyboard keys
        self.note_mapping = {
            81: (0, "1"),
            82: (1, "2"),
            83: (2, "3"),
            84: (3, "4"),
            85: (4, "6"),
            86: (5, "E"),
            87: (6, "R"),
            88: (7, "T"),
        }
        
        # A queue for delayed, temporally ordered color commands for the device
        self.off_queue = Queue.Queue()
        thread.start_new_thread(self.off_thread, ())
    #
    
    def off_thread(self):
        while 1:
            t, note, color = self.off_queue.get()
            now = time.clock()
            delta = t - now
            
            # Sleep if not late
            if delta>0:
                time.sleep(delta)
            #
            
            self.set_led(note, color.r, color.g, color.b)
        #
    #
    
    def open(self):
        self.input = mido.open_input(self.device_in)
        self.output = mido.open_output(self.device_out)
    #
    
    def close(self):
        self.input.close()
        self.output.close()
    #
    
    def clear_all(self):
        """Clear all the leds in the main area"""
        
        data = [0, 32, 41, 2, 16, 11]
        
        # Multiple color commands can be batched together
        for i in range(11, 89):
            data.extend([i, 0,0,0])
        #
        
        msg = mido.Message('sysex', data=data)
        self.output.send(msg)
    #

    def set_led(self, i, r,g,b):
        """Set a specific led to a specific color"""
    
        # The supported color range is 0...127
        msg = mido.Message('sysex', data=[0, 32, 41, 2, 16, 11, i, r/2,g/2,b/2])
        self.output.send(msg)
    #
    
    def do(self):
        """Main entry point of sorts"""
    
        self.clear_all()
        
        # Set the initial colors
        for i in range(8):
            color = colors[i]
            color = [color.r, color.g, color.b]
            self.set_led(81+i, *color)
        #
        
        # Start reading the notes
        for msg in self.input:
            if msg.type == 'note_on':
                # Vel=0 means off when using the programmer mode
                if msg.note in self.note_mapping and msg.velocity > 0:
                    index, key = self.note_mapping[msg.note]
                    
                    # Send the key press
                    Auto.ControlSend ("[handle:%s]"%hwnd, "", "", key)
                    
                    # Feedback for pressing the keys
                    # Set to a brigter color, and then back after a delay
                    white = Color(255,255,255)
                    real_color = colors[index]
                    color = real_color.mix(white, 0.2)
                    self.set_led(msg.note, color.r, color.g, color.b)
                    self.off_queue.put((time.clock()+0.07, msg.note, real_color))
                    
                    # Print the timecode and the note;
                    # because why not?
                    print "%4.3f %s" % (time.clock(), key)
        #
    #
#


if __name__ == '__main__':
    # Depending on your installation, you might want to tweak this
    mido.set_backend('mido.backends.rtmidi')
    
    # Print device names for no particular reason
    print mido.get_input_names()
    print mido.get_output_names()
    
    # Being extremely lazy and using AutoIt for sending the keys
    from win32com.client import Dispatch
    Auto = Dispatch("AutoItX3.Control")
    hwnd = Auto.WinGetHandle("Guild Wars 2", "");
    
    bt = BellThing()
    bt.open()
    
    try:
        bt.do()
    finally:
        bt.clear_all()
        bt.close()
    #
#

